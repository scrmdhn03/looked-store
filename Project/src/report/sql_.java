/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import crud.*;

/**
 *
 * @author MartaSayoer19
 */
public interface sql_ {
    //FORM REPORT
    void insert_table(String sql);
    void viewed_table(String sql);
    void update_table(String sql);
    void delete_table(String sql);
    
    abstract String reset_txt();
    abstract String reset_tbl();
    abstract String reset_btn();
    abstract String display_tbl(String sql);
    
    abstract String check_idtrans(String f1, String f3);
    abstract String check_idproduct();
    abstract String check_idcustomer();
    abstract String check_idemployee();
    abstract String check_idsupplier();
}
