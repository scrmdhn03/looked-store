package project;

import java.sql.*;
public class NewClass {
    
    //ini nanti dirubah sesuai nama databasenya
    private static String url = "jdbc:mysql://localhost:3306/lookeddb?zeroDateTimeBehavior=convertToNull";
    private static String driverName ="com.mysql.cj.jdbc.Driver"; 
    private static String userName = "root";
    private static String password = "";
    private static Connection con;
    public static Connection getConnection(){
       try {
           Class.forName(driverName);
           try{
               con = DriverManager.getConnection(url, userName, password);
           } catch (Exception e) {
               System.out.println("Failed to Create the database connection");
           }
       }catch (ClassNotFoundException ex) {
          System.out.println("Driver not found ");
       }
       return con;
   }
    
}