/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

/**
 *
 */
public interface sql_ { // karena diseluruh method pada kelas interface tidak mempunyai variable biar bisa di terapin di semua class, tanpa nerapin inheritance (extends). 
    void insert_btn(String sql);
    void viewed_table(String sql);
    void update_btn(String sql);
    void delete_btn(String sql);
    abstract String reset_tbl();
    abstract String reset_btn();
    abstract String display_tbl(String sql);
    abstract String check_idtrans(); // lo tau definisi jajan, tapi lo gatau object jajan itu apa, atau lo bakal jajan apaaa
    abstract String check_idproduct();
    abstract String check_idcustomer();
    abstract String check_idemployee();
    abstract String check_idsupplier();
    
    // form customer a.k.a Sabrina
    abstract String add_btn(String id, String nama, String email, String address, String phone, String Gender);
    
    //form employee a.k.a Suci
    abstract String add_btn(String id, String nama, String email, String address, String phone, String position, String Gender);
   
    // //form login a.k.a Suci
    abstract String insert_btn(String username, String password);
    //abstract String edit_btn(String id, String nama, String email, String address, String phone, String Gender);
    
    
}
