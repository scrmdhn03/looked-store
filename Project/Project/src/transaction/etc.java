/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transaction;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import koneksi.KONEKSI;

/**
 *
 * @author MartaSayoer19
 */
public class etc extends gado2 {
    ResultSet rs1;
    String f0000=null;
    public etc(String idtrans){
        String f0=null;
        String f00=null;
        String f000=null;
        
        try {
            String sq = "SELECT * FROM transaction WHERE id_transaction = '"+idtrans+"';";
            rs1 = holysql(sq);
            while(rs1.next()){
                String f1 = rs1.getString("id_transaction");
                String f2 = rs1.getString("id_employee");
                String f3 = rs1.getString("id_customer");
                String f4 = rs1.getString("date_transcaction");
                //System.out.println(f1+f2+f3+f4);
                f0=headerstru(f1,f2,f3,f4);
                //System.out.println(f0);
            }
        } catch (SQLException ex) {
            Logger.getLogger(etc.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            int i=1;
            String sq = "SELECT c.name_product AS a1 , b.qty AS a2 ,  c.price_product AS a3 , (b.qty*c.price_product) AS a4\n" +
"FROM transaction a INNER JOIN transaction_detail b INNER JOIN product c INNER JOIN employee d\n" +
"WHERE a.id_transaction = b.id_transaction AND b.id_product = c.id_product AND a.id_employee = d.id_employee \n" +
"AND a.id_transaction=\""+idtrans+"\"";
            rs1 = holysql(sq);
            StringBuilder sb = new StringBuilder();
            while(rs1.next()){
                String f1 = rs1.getString("a1");
                String f2 = rs1.getString("a2");
                String f3 = rs1.getString("a3");
                String f4 = rs1.getString("a4");
                //System.out.println(f1+f2+f3+f4);
                
        
                f00=centerstru(String.valueOf(i),f1,f2,f3,f4);
                sb.append(f00);
                //System.out.println(f00);
                i++;
            }
            f00 = sb.toString();
        } catch (SQLException ex) {
            Logger.getLogger(etc.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            String sq = "SELECT SUM(b.price_product*a.qty) AS total "
                    + "FROM transaction_detail a INNER JOIN product b "
                    + "WHERE a.id_product = b.id_product AND a.id_transaction = \""+idtrans+"\"";
            rs1 = holysql(sq);
            while(rs1.next()){
                String f1 = rs1.getString("total");
                //System.out.println(f1);
                f000=footerstru(f1);
                //System.out.println(f000);
            }
        } catch (SQLException ex) {
            Logger.getLogger(etc.class.getName()).log(Level.SEVERE, null, ex);
        }
        f0000=f0+"\n"+f00+"\n"+f000+"\n";
        //System.out.println(f0000);
    }
    
    public etc(String idtrans, String cash, String payback){
        String f0=null;
        String f00=null;
        String f000=null;
        
        try {
            String sq = "SELECT * FROM transaction WHERE id_transaction = '"+idtrans+"';";
            rs1 = holysql(sq);
            while(rs1.next()){
                String f1 = rs1.getString("id_transaction");
                String f2 = rs1.getString("id_employee");
                String f3 = rs1.getString("id_customer");
                String f4 = rs1.getString("date_transcaction");
                //System.out.println(f1+f2+f3+f4);
                f0=headerstru(f1,f2,f3,f4);
                //System.out.println(f0);
            }
        } catch (SQLException ex) {
            Logger.getLogger(etc.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            int i=1;
            String sq = "SELECT c.name_product AS a1 , b.qty AS a2 ,  c.price_product AS a3 , (b.qty*c.price_product) AS a4\n" +
"FROM transaction a INNER JOIN transaction_detail b INNER JOIN product c INNER JOIN employee d\n" +
"WHERE a.id_transaction = b.id_transaction AND b.id_product = c.id_product AND a.id_employee = d.id_employee \n" +
"AND a.id_transaction=\""+idtrans+"\"";
            rs1 = holysql(sq);
            StringBuilder sb = new StringBuilder();
            while(rs1.next()){
                String f1 = rs1.getString("a1");
                String f2 = rs1.getString("a2");
                String f3 = rs1.getString("a3");
                String f4 = rs1.getString("a4");
                //System.out.println(f1+f2+f3+f4);
                
        
                f00=centerstru(String.valueOf(i),f1,f2,f3,f4);
                sb.append(f00);
                //System.out.println(f00);
                i++;
            }
            f00 = sb.toString();
        } catch (SQLException ex) {
            Logger.getLogger(etc.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            String sq = "SELECT SUM(b.price_product*a.qty) AS total "
                    + "FROM transaction_detail a INNER JOIN product b "
                    + "WHERE a.id_product = b.id_product AND a.id_transaction = \""+idtrans+"\"";
            rs1 = holysql(sq);
            while(rs1.next()){
                String f1 = rs1.getString("total");
                //System.out.println(f1);
                f000=footerstru(f1,cash,payback);
                //System.out.println(f000);
            }
        } catch (SQLException ex) {
            Logger.getLogger(etc.class.getName()).log(Level.SEVERE, null, ex);
        }
        f0000=f0+"\n"+f00+"\n"+f000+"\n";
        //System.out.println(f0000);
    }
    
    
    @Override
    String viewstruk() {
        //System.out.println(rs1);
        return f0000;
    }
    
    final ResultSet holysql(String sql) throws SQLException{
        conn = new KONEKSI().getConnection();
        stmt = conn.createStatement();
        rs = stmt.executeQuery(sql);
        //System.out.println(rs);
        return rs;
        //conn.close();
    }
    
    final String headerstru(String f1, String f2, String f3, String f4){
        String f0 = "\n"+
"============================================================\n" +
"IDTrans\t: "+f1+"\n"+
"Cashier\t: "+f2+"\n"+
"Customert   : "+f3+"\n"+
"Date   \t: "+f4+"\n"+
"============================================================" + 
                "";
        return f0;
    }
    
    public final String centerstru(String f1, String f2, String f3, String f4, String f5){
        String f0 = "\n"+ 
"No.["+f1+"]\n" +
"Item Product\t: "+f2+"\n" +
"PriceProduct\t: Rp."+f4+",-\n" +
"Quantity Pro\t: "+f3+"\n" +
"Subtotal Pro\t: Rp."+f5+",-\n" + 
                "";
        return f0;
    }
    
    public final String footerstru(String f1){
        String f0 = "" +
"============================================================\n" +
"Total\t: Rp."+f1+",-\n" +
"============================================================\n" +
                    "\n";
        return f0;
    }
    public final String footerstru(String f1, String f2 , String f3){
        String f0 = "" +
"============================================================\n" +
"Total\t: Rp."+f1+",-\n" + 
"Pay\t: Rp."+f2+",-\n" +
"C.back\t: Rp."+f3+",-\n" +
"============================================================\n" +
"    \"Kamsahamida, TerimaKasih Silahkan Datang Kembali\"\n" +
"============================================================\n"+
"============================================================\n" +
                    "\n";
        return f0;
    }
    
    
    public etc(String idtrans, String sql){
        this.idtrans=idtrans;
        this.sql=sql;
    }
    
    @Override
    String createtru() {
        //System.out.println(idtrans+sql);
        return null;
    }

}
