package transaction;
import java.sql.*;
import java.time.LocalDate;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import project.KONEKSI;
import crud.*;
import java.awt.print.PrinterException;
import java.util.logging.Level;
import java.util.logging.Logger;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author sabri
 */
public class transactions extends javax.swing.JFrame implements sql_ {

    /**
     * Creates new form transactions
     */
    
    static Connection conn;
    static Statement stmt;
    static ResultSet rs;
    static String sql;
    static PreparedStatement ps;
    
    
    public String checktotal(String idtrans){
        String  f1 = null;
        try {
            conn = new KONEKSI().getConnection();
            stmt = conn.createStatement();
            sql = "SELECT SUM(b.price_product*a.qty) AS total FROM transaction_detail a INNER JOIN product b WHERE a.id_product = b.id_product AND a.id_transaction = \""+idtrans+"\"";
            rs = stmt.executeQuery(sql);
            //System.out.println(choice2.getSelectedItem());
            while(rs.next()){
                f1 = rs.getString("total");
                //System.out.println(f1);
            }
            //rs.close();
            //stmt.close();
            conn.close();
        } catch (SQLException ex){
            System.out.println(ex);
        }
        return f1;
    }
    
    public String checkidproduct(String product){
        String  f1 = null;
        try {
            conn = new KONEKSI().getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT id_product FROM `product` WHERE `name_product` = '" + product + "';");
            //System.out.println(choice2.getSelectedItem());
            while(rs.next()){
                f1 = rs.getString("id_product");
                //System.out.println(f1);
            }
            //rs.close();
            //stmt.close();
            conn.close();
        } catch (SQLException ex){
            System.out.println(ex);
        }
        return f1;
    }
    
    public String checkprice(String product){
        String  f1 = null; 
        try {
            conn = new KONEKSI().getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT price_product FROM `product` WHERE `name_product` = '" + product + "';");
            //System.out.println(choice2.getSelectedItem());
            while(rs.next()){
                f1 = rs.getString("price_product");
                System.out.println(f1);
            }
            //rs.close();
            //stmt.close();
            conn.close();
        } catch (SQLException ex){
            System.out.println(ex);
        }
        return f1;
    }
    
    public String checkstock(String product){
        String  f1 = null; 
        try {
            conn = new KONEKSI().getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT stock_product FROM `product` WHERE `name_product` = '" + product + "';");
            while(rs.next()){
                f1 = rs.getString("stock_product");
                System.out.println(f1);
            }
            conn.close();
        } catch (SQLException ex){
            System.out.println(ex);
        }
        return f1;
    }
    
    public String checkidnow(){
        String f1 = null;
        try{
            conn = new KONEKSI().getConnection();
            String query = "SELECT MAX(id_transaction) AS id_transaction FROM transaction";
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()){
                f1 = rs.getString("id_transaction");
                //System.out.print(f1);
            }
            conn.close();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return f1;
    }
    
    public String checkid(){
        String f1 = null;
        int f2=0;
        try{
            conn = new KONEKSI().getConnection();
            String query = "SELECT MAX(id_transaction) AS id_transaction FROM transaction";
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()){
                f1 = rs.getString("id_transaction");
                //System.out.println(f1);
                if (f1==null){
                    //System.out.println("null gan");
                } else {
                    //System.out.println("ada! gan");
                    //System.out.print(f1.substring(1));
                    f2 = Integer.parseInt(f1.substring(1));
                }
                //f2 = Integer.parseInt(f1.substring(1));
                if ( f1 == null || f2 == 0) {
                    f1 = "T"+"0"+"0"+"0"+"0"+"1";
                } else if (f2 == 9){
                    f1 = "T"+"0"+"0"+"0"+"1"+"0";
                } else if (f2 == 99){
                    f1 = "T"+"0"+"0"+"1"+"0"+"0";
                } else if (f2 == 999){
                    f1 = "T"+"0"+"1"+"0"+"0"+"0";
                } else if (f2 == 9999){
                    f1 = "T"+"1"+"0"+"0"+"0"+"0";
                } else if (f2 < 9){
                    f1 = "T"+"0"+"0"+"0"+"0"+(f2+1);
                } else if (f2 < 99 && f2 > 9) {
                    f1 = "T"+"0"+"0"+"0"+(f2+1);
                } else if (f2 < 999 && f2 > 99) {
                    f1 = "T"+"0"+"0"+(f2+1);
                } else if (f2 < 9999 && f2 > 999) {
                    f1 = "T"+"0"+(f2+1);
                } else if (f2 < 99999 && f2 > 9999) {
                    f1 = "T"+(f2+1);
                } else {
                    System.out.print("EXCEED Limit Pfft 6 digit bruh");
                }
                System.out.print(f2);
                System.out.print(f1);
            }
            //rs.close();
            //stmt.close();
            conn.close();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return f1;
    }
    
    public void table_plis(String id){
        Object [] kolom = {"Product", "Price", "Qty", "Subtotal"};
        DefaultTableModel view = (DefaultTableModel) table_transaction.getModel();
        view.setColumnIdentifiers(kolom);
        view.setRowCount(0);
        String f1,f2,f3,f4;
        try 
        {
            conn = new KONEKSI().getConnection();
            String nampilin = "SELECT b.name_product, b.price_product, a.qty, (b.price_product*a.qty) AS subtotal FROM transaction_detail a INNER JOIN product b WHERE a.id_product = b.id_product AND a.id_transaction = \""+id+"\"";
            stmt = conn.createStatement();
            rs = stmt.executeQuery(nampilin);
            int i = 0;
            while (rs.next())
            {
                f1=rs.getString("name_product");
                f2=rs.getString("price_product");
                f3=rs.getString("qty");
                f4=rs.getString("subtotal");
                view.addRow( new Object[] {f1,f2,f3,f4});
                i++;
            }
            if (i < 1)
            {
                //JOptionPane.showMessageDialog(null, "NO RECORD FOUND","ERROR",JOptionPane.ERROR_MESSAGE);
            }
            
        } catch (SQLException e) 
        {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    
    public void display_combobox(String jenis){
        System.out.println("Mencoba Nampilin Data ke Table");
        try {
            conn = new KONEKSI().getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM `product` WHERE `type_product` = '" + jenis + "';");
            while(rs.next()){
                String  f1=rs.getString("name_product");
                System.out.println(f1);
                if ( "Pants".equals(jenis) ){
                    choice1.addItem(f1);
                } else {
                    choice2.addItem(f1);
                }
            }
            conn.close();
        }catch(SQLException ex){
            System.out.println(ex);
        }
    }

    public static void insert(){
        System.out.println("CRUD");
        try {
            conn = new KONEKSI().getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("");
                    
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }
    
    public transactions() {
        initComponents();
        //crud();
        System.out.println(Date.valueOf(LocalDate.now()).toString());
        
        //display_table();
        display_combobox("Pants");
        display_combobox("Clothes");
        txt_idtrans.setText(checkid());
        txt_idtransnow.setText(checkidnow());
        table_plis(txt_idtrans.getText());
        reset_txt();
        //checkid();
        test();
        
    }
    
    public static void test(){
        etc struk = new etc("T","R");
        //struk.etc(sql);
        struk.createtru();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        panel1 = new java.awt.Panel();
        jSeparator5 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        txt_idtrans = new javax.swing.JTextField();
        jSeparator10 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        txt_idemp = new javax.swing.JTextField();
        jSeparator11 = new javax.swing.JSeparator();
        jLabel9 = new javax.swing.JLabel();
        txt_total = new javax.swing.JTextField();
        jSeparator16 = new javax.swing.JSeparator();
        jLabel10 = new javax.swing.JLabel();
        txt_pembayaran = new javax.swing.JTextField();
        jSeparator17 = new javax.swing.JSeparator();
        jLabel12 = new javax.swing.JLabel();
        txt_kembalian = new javax.swing.JTextField();
        jSeparator20 = new javax.swing.JSeparator();
        btn_reset = new javax.swing.JButton();
        btn_add = new javax.swing.JButton();
        btn_del = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        conn = new KONEKSI().getConnection();
        table_transaction = new javax.swing.JTable();
        btn_ok = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        txt_idcust = new javax.swing.JTextField();
        jSeparator12 = new javax.swing.JSeparator();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        choice1 = new java.awt.Choice();
        txt_price = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jSeparator26 = new javax.swing.JSeparator();
        txt_qty = new javax.swing.JTextField();
        jSeparator25 = new javax.swing.JSeparator();
        txt_idpants = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jSeparator32 = new javax.swing.JSeparator();
        jSeparator33 = new javax.swing.JSeparator();
        jSeparator27 = new javax.swing.JSeparator();
        txt_stock = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        choice2 = new java.awt.Choice();
        txt_price2 = new javax.swing.JTextField();
        jSeparator28 = new javax.swing.JSeparator();
        txt_qty2 = new javax.swing.JTextField();
        txt_idclothes = new javax.swing.JTextField();
        jSeparator29 = new javax.swing.JSeparator();
        jSeparator30 = new javax.swing.JSeparator();
        jSeparator31 = new javax.swing.JSeparator();
        jSeparator34 = new javax.swing.JSeparator();
        txt_stock2 = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        area = new javax.swing.JTextArea();
        txt_1 = new javax.swing.JTextField();
        txt_2 = new javax.swing.JTextField();
        txt_3 = new javax.swing.JTextField();
        txt_0 = new javax.swing.JTextField();
        txt_ = new javax.swing.JTextField();
        txt_idtransnow = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel4.setBackground(new java.awt.Color(0, 0, 0));

        panel1.setBackground(new java.awt.Color(0, 0, 0));
        panel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        panel1.add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 70, 920, 20));

        jLabel2.setFont(new java.awt.Font("Yu Gothic UI Semilight", 0, 48)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("TRANSACTION ");
        panel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 10, 330, 70));

        txt_idtrans.setBackground(new java.awt.Color(115, 96, 91));
        txt_idtrans.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_idtrans.setText("idtrans");
        txt_idtrans.setToolTipText("");
        txt_idtrans.setBorder(null);
        txt_idtrans.setDisabledTextColor(new java.awt.Color(204, 204, 204));
        txt_idtrans.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_idtransActionPerformed(evt);
            }
        });
        panel1.add(txt_idtrans, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 120, 210, 30));
        panel1.add(jSeparator10, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 150, 210, 10));

        jLabel5.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("ID EMPLOYEE");
        panel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 100, -1, 20));

        txt_idemp.setBackground(new java.awt.Color(115, 96, 91));
        txt_idemp.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_idemp.setText("E001");
        txt_idemp.setToolTipText("");
        txt_idemp.setBorder(null);
        txt_idemp.setDisabledTextColor(new java.awt.Color(204, 204, 204));
        txt_idemp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_idempActionPerformed(evt);
            }
        });
        panel1.add(txt_idemp, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 120, 210, 30));
        panel1.add(jSeparator11, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 150, 210, 10));

        jLabel9.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("TOTAL");
        panel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 370, -1, 30));

        txt_total.setBackground(new java.awt.Color(115, 96, 91));
        txt_total.setFont(new java.awt.Font("Segoe UI Light", 0, 18)); // NOI18N
        txt_total.setForeground(new java.awt.Color(255, 255, 255));
        txt_total.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_total.setToolTipText("");
        txt_total.setBorder(null);
        txt_total.setCaretColor(new java.awt.Color(255, 255, 255));
        txt_total.setDisabledTextColor(new java.awt.Color(204, 204, 204));
        txt_total.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_totalActionPerformed(evt);
            }
        });
        panel1.add(txt_total, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 370, 250, 30));
        panel1.add(jSeparator16, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 400, 320, 20));

        jLabel10.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("CASH");
        panel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 420, 60, 30));

        txt_pembayaran.setBackground(new java.awt.Color(115, 96, 91));
        txt_pembayaran.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        txt_pembayaran.setForeground(new java.awt.Color(255, 255, 255));
        txt_pembayaran.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_pembayaran.setToolTipText("");
        txt_pembayaran.setBorder(null);
        txt_pembayaran.setCaretColor(new java.awt.Color(255, 255, 255));
        txt_pembayaran.setDisabledTextColor(new java.awt.Color(204, 204, 204));
        txt_pembayaran.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_pembayaranActionPerformed(evt);
            }
        });
        txt_pembayaran.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_pembayaranKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_pembayaranKeyTyped(evt);
            }
        });
        panel1.add(txt_pembayaran, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 420, 250, 30));
        panel1.add(jSeparator17, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 450, 320, 20));

        jLabel12.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel12.setText("REFUND");
        panel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 470, -1, 30));

        txt_kembalian.setBackground(new java.awt.Color(115, 96, 91));
        txt_kembalian.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        txt_kembalian.setForeground(new java.awt.Color(255, 255, 255));
        txt_kembalian.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_kembalian.setToolTipText("");
        txt_kembalian.setBorder(null);
        txt_kembalian.setCaretColor(new java.awt.Color(255, 255, 255));
        txt_kembalian.setDisabledTextColor(new java.awt.Color(204, 204, 204));
        txt_kembalian.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_kembalianActionPerformed(evt);
            }
        });
        panel1.add(txt_kembalian, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 470, 250, 30));
        panel1.add(jSeparator20, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 500, 320, 20));

        btn_reset.setIcon(new javax.swing.ImageIcon(getClass().getResource("/project/189_key.png"))); // NOI18N
        btn_reset.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_resetActionPerformed(evt);
            }
        });
        panel1.add(btn_reset, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 440, 70, 70));

        btn_add.setIcon(new javax.swing.ImageIcon(getClass().getResource("/project/066_key.png"))); // NOI18N
        btn_add.setToolTipText("Adding Product");
        btn_add.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_addActionPerformed(evt);
            }
        });
        panel1.add(btn_add, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 360, 70, 70));
        btn_add.getAccessibleContext().setAccessibleName("TTTE");

        btn_del.setIcon(new javax.swing.ImageIcon(getClass().getResource("/project/068_key.png"))); // NOI18N
        btn_del.setToolTipText("Delete Product");
        btn_del.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_del.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_delActionPerformed(evt);
            }
        });
        panel1.add(btn_del, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 440, 70, 70));

        table_transaction.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        table_transaction.setFocusable(false);
        table_transaction.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table_transactionMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(table_transaction);

        panel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 180, 560, 170));

        btn_ok.setIcon(new javax.swing.ImageIcon(getClass().getResource("/project/187_key.png"))); // NOI18N
        btn_ok.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_ok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_okActionPerformed(evt);
            }
        });
        panel1.add(btn_ok, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 360, 70, 70));

        jLabel6.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("ID CUSTOMER");
        panel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 100, -1, 20));

        txt_idcust.setBackground(new java.awt.Color(115, 96, 91));
        txt_idcust.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_idcust.setText("idcusts");
        txt_idcust.setToolTipText("");
        txt_idcust.setBorder(null);
        txt_idcust.setDisabledTextColor(new java.awt.Color(204, 204, 204));
        txt_idcust.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_idcustActionPerformed(evt);
            }
        });
        panel1.add(txt_idcust, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 120, 210, 30));
        panel1.add(jSeparator12, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 150, 210, 10));

        jTabbedPane1.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        jTabbedPane1.setFocusable(false);

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));
        jPanel1.setLayout(null);

        jLabel26.setBackground(new java.awt.Color(255, 255, 255));
        jLabel26.setFont(new java.awt.Font("Segoe UI Light", 0, 48)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(255, 255, 255));
        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel26.setText("PANTS");
        jPanel1.add(jLabel26);
        jLabel26.setBounds(20, 20, 230, 50);

        choice1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        choice1.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        choice1.setName(""); // NOI18N
        choice1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                choice1MouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                choice1MousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                choice1MouseReleased(evt);
            }
        });
        choice1.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentHidden(java.awt.event.ComponentEvent evt) {
                choice1ComponentHidden(evt);
            }
            public void componentMoved(java.awt.event.ComponentEvent evt) {
                choice1ComponentMoved(evt);
            }
            public void componentResized(java.awt.event.ComponentEvent evt) {
                choice1ComponentResized(evt);
            }
        });
        choice1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                choice1ItemStateChanged(evt);
            }
        });
        choice1.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
                choice1CaretPositionChanged(evt);
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
            }
        });
        choice1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                choice1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                choice1FocusLost(evt);
            }
        });
        choice1.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                choice1PropertyChange(evt);
            }
        });
        jPanel1.add(choice1);
        choice1.setBounds(20, 90, 230, 30);

        txt_price.setEditable(false);
        txt_price.setBackground(new java.awt.Color(115, 96, 91));
        txt_price.setForeground(new java.awt.Color(255, 255, 255));
        txt_price.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_price.setText("txt_price");
        txt_price.setToolTipText("");
        txt_price.setBorder(null);
        txt_price.setDisabledTextColor(new java.awt.Color(255, 255, 255));
        txt_price.setFocusable(false);
        txt_price.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_priceActionPerformed(evt);
            }
        });
        jPanel1.add(txt_price);
        txt_price.setBounds(150, 170, 110, 30);

        jLabel21.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(255, 255, 255));
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("ID");
        jPanel1.add(jLabel21);
        jLabel21.setBounds(10, 130, 80, 30);
        jPanel1.add(jSeparator26);
        jSeparator26.setBounds(20, 200, 230, 10);

        txt_qty.setBackground(new java.awt.Color(115, 96, 91));
        txt_qty.setFont(new java.awt.Font("Segoe UI Semilight", 0, 13)); // NOI18N
        txt_qty.setForeground(new java.awt.Color(255, 255, 255));
        txt_qty.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_qty.setText("0");
        txt_qty.setToolTipText("");
        txt_qty.setBorder(null);
        txt_qty.setCaretColor(new java.awt.Color(255, 255, 255));
        txt_qty.setDisabledTextColor(new java.awt.Color(204, 204, 204));
        txt_qty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_qtyActionPerformed(evt);
            }
        });
        txt_qty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_qtyKeyReleased(evt);
            }
        });
        jPanel1.add(txt_qty);
        txt_qty.setBounds(20, 250, 230, 30);
        jPanel1.add(jSeparator25);
        jSeparator25.setBounds(20, 280, 230, 10);

        txt_idpants.setEditable(false);
        txt_idpants.setBackground(new java.awt.Color(115, 96, 91));
        txt_idpants.setForeground(new java.awt.Color(255, 255, 255));
        txt_idpants.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_idpants.setText("txt_idpants");
        txt_idpants.setBorder(null);
        txt_idpants.setFocusable(false);
        txt_idpants.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_idpantsActionPerformed(evt);
            }
        });
        jPanel1.add(txt_idpants);
        txt_idpants.setBounds(10, 170, 80, 30);

        jLabel20.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel20.setText("Quantity");
        jPanel1.add(jLabel20);
        jLabel20.setBounds(10, 210, 250, 30);
        jPanel1.add(jSeparator32);
        jSeparator32.setBounds(10, 240, 250, 10);
        jPanel1.add(jSeparator33);
        jSeparator33.setBounds(10, 160, 250, 10);
        jPanel1.add(jSeparator27);
        jSeparator27.setBounds(10, 80, 250, 20);

        txt_stock.setEditable(false);
        txt_stock.setBackground(new java.awt.Color(115, 96, 91));
        txt_stock.setForeground(new java.awt.Color(255, 255, 255));
        txt_stock.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_stock.setText("txt_stock");
        txt_stock.setBorder(null);
        txt_stock.setFocusable(false);
        txt_stock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_stockActionPerformed(evt);
            }
        });
        jPanel1.add(txt_stock);
        txt_stock.setBounds(90, 170, 60, 30);

        jLabel25.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(255, 255, 255));
        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel25.setText("Price");
        jPanel1.add(jLabel25);
        jLabel25.setBounds(150, 130, 120, 30);

        jLabel27.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(255, 255, 255));
        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel27.setText("Stock");
        jPanel1.add(jLabel27);
        jLabel27.setBounds(90, 130, 60, 30);

        jTabbedPane1.addTab("tab1", jPanel1);

        jPanel2.setBackground(new java.awt.Color(0, 0, 0));
        jPanel2.setLayout(null);

        jLabel22.setBackground(new java.awt.Color(255, 255, 255));
        jLabel22.setFont(new java.awt.Font("Segoe UI Light", 0, 48)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel22.setText("CLOTHES");
        jPanel2.add(jLabel22);
        jLabel22.setBounds(20, 20, 230, 50);

        choice2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        choice2.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        choice2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                choice2MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                choice2MouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                choice2MousePressed(evt);
            }
        });
        choice2.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentHidden(java.awt.event.ComponentEvent evt) {
                choice2ComponentHidden(evt);
            }
        });
        choice2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                choice2FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                choice2FocusLost(evt);
            }
        });
        jPanel2.add(choice2);
        choice2.setBounds(20, 90, 230, 30);

        txt_price2.setEditable(false);
        txt_price2.setBackground(new java.awt.Color(115, 96, 91));
        txt_price2.setForeground(new java.awt.Color(255, 255, 255));
        txt_price2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_price2.setText("txt_price2");
        txt_price2.setToolTipText("");
        txt_price2.setBorder(null);
        txt_price2.setCaretColor(new java.awt.Color(255, 255, 255));
        txt_price2.setDisabledTextColor(new java.awt.Color(204, 204, 204));
        txt_price2.setFocusable(false);
        txt_price2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_price2ActionPerformed(evt);
            }
        });
        jPanel2.add(txt_price2);
        txt_price2.setBounds(150, 170, 110, 30);
        jPanel2.add(jSeparator28);
        jSeparator28.setBounds(10, 240, 250, 20);

        txt_qty2.setBackground(new java.awt.Color(115, 96, 91));
        txt_qty2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_qty2.setForeground(new java.awt.Color(255, 255, 255));
        txt_qty2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_qty2.setText("0");
        txt_qty2.setToolTipText("");
        txt_qty2.setBorder(null);
        txt_qty2.setCaretColor(new java.awt.Color(255, 255, 255));
        txt_qty2.setDisabledTextColor(new java.awt.Color(204, 204, 204));
        txt_qty2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_qty2ActionPerformed(evt);
            }
        });
        txt_qty2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_qty2KeyReleased(evt);
            }
        });
        jPanel2.add(txt_qty2);
        txt_qty2.setBounds(20, 250, 230, 30);

        txt_idclothes.setEditable(false);
        txt_idclothes.setBackground(new java.awt.Color(115, 96, 91));
        txt_idclothes.setForeground(new java.awt.Color(255, 255, 255));
        txt_idclothes.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_idclothes.setText("txt_idclothes");
        txt_idclothes.setBorder(null);
        txt_idclothes.setFocusable(false);
        txt_idclothes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_idclothesActionPerformed(evt);
            }
        });
        jPanel2.add(txt_idclothes);
        txt_idclothes.setBounds(10, 170, 80, 30);
        jPanel2.add(jSeparator29);
        jSeparator29.setBounds(20, 280, 230, 20);
        jPanel2.add(jSeparator30);
        jSeparator30.setBounds(10, 160, 250, 20);
        jPanel2.add(jSeparator31);
        jSeparator31.setBounds(20, 200, 230, 20);
        jPanel2.add(jSeparator34);
        jSeparator34.setBounds(10, 80, 250, 20);

        txt_stock2.setEditable(false);
        txt_stock2.setBackground(new java.awt.Color(115, 96, 91));
        txt_stock2.setForeground(new java.awt.Color(255, 255, 255));
        txt_stock2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_stock2.setText("txt_stock2");
        txt_stock2.setBorder(null);
        txt_stock2.setFocusable(false);
        txt_stock2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_stock2ActionPerformed(evt);
            }
        });
        jPanel2.add(txt_stock2);
        txt_stock2.setBounds(90, 170, 60, 30);

        jLabel28.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(255, 255, 255));
        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel28.setText("ID");
        jPanel2.add(jLabel28);
        jLabel28.setBounds(10, 130, 80, 30);

        jLabel29.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(255, 255, 255));
        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel29.setText("Stock");
        jPanel2.add(jLabel29);
        jLabel29.setBounds(90, 130, 60, 30);

        jLabel30.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(255, 255, 255));
        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel30.setText("Price");
        jPanel2.add(jLabel30);
        jLabel30.setBounds(150, 130, 120, 30);

        jLabel24.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(255, 255, 255));
        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel24.setText("Quantity");
        jPanel2.add(jLabel24);
        jLabel24.setBounds(10, 210, 250, 30);

        jTabbedPane1.addTab("tab2", jPanel2);

        jPanel3.setBackground(new java.awt.Color(0, 0, 0));

        area.setBackground(new java.awt.Color(204, 255, 204));
        area.setColumns(20);
        area.setRows(5);
        area.setTabSize(6);
        area.setText("PAIJO");
        area.setBorder(null);
        jScrollPane2.setViewportView(area);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 275, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 255, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 422, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        jTabbedPane1.addTab("tab3", jPanel3);

        panel1.add(jTabbedPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 180, 280, 450));
        jTabbedPane1.getAccessibleContext().setAccessibleName("");

        txt_1.setText("jTextField1");
        txt_1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_1ActionPerformed(evt);
            }
        });
        panel1.add(txt_1, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 580, -1, -1));

        txt_2.setText("jTextField1");
        panel1.add(txt_2, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 600, -1, -1));

        txt_3.setText("jTextField1");
        panel1.add(txt_3, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 620, -1, -1));

        txt_0.setText("jTextField1");
        panel1.add(txt_0, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 560, -1, -1));

        txt_.setText("jTextField1");
        panel1.add(txt_, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 540, -1, -1));

        txt_idtransnow.setText("jTextField1");
        panel1.add(txt_idtransnow, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 540, -1, -1));

        jLabel3.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("ID TRANSACTION ");
        panel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 100, -1, -1));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panel1, javax.swing.GroupLayout.PREFERRED_SIZE, 930, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(20, Short.MAX_VALUE)
                .addComponent(panel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel4, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_kembalianActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_kembalianActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_kembalianActionPerformed

    private void txt_pembayaranActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_pembayaranActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_pembayaranActionPerformed

    private void txt_totalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_totalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_totalActionPerformed

    private void txt_idempActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_idempActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_idempActionPerformed

    private void txt_idtransActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_idtransActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_idtransActionPerformed

    private void txt_qtyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_qtyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_qtyActionPerformed

    private void txt_priceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_priceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_priceActionPerformed

    private void txt_qty2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_qty2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_qty2ActionPerformed

    private void txt_price2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_price2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_price2ActionPerformed

    private void btn_resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_resetActionPerformed
        reset_txt();
    }//GEN-LAST:event_btn_resetActionPerformed

    private void btn_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addActionPerformed
        
        txt_idtransnow.setText(checkidnow());
        String f0 = txt_idtransnow.getText();
        String f1 = txt_idtrans.getText();
        if (!f0.equals(f1)) {
            String f2 = txt_idemp.getText();
            String f3 = txt_idcust.getText().trim(); System.out.println(f3);
            if (f3==null || "".equals(f3)){
                f3 = "NULL";
            } else {
                f3 = "'"+f3+"'";
            }
            String f4 = Date.valueOf(LocalDate.now()).toString();
            try {
                conn = new KONEKSI().getConnection();
                stmt = conn.createStatement();
                //System.out.print(checkidnow());
                //rs = stmt.executeQuery("");
                sql = "INSERT INTO `transaction` (`id_transaction`, `id_employee`, `id_customer`, `date_transcaction`) VALUES ('"+f1+"', '"+f2+"', "+f3+", '"+f4+"')";
                int i = stmt.executeUpdate(sql);
                if (i != 0)
                {
                //JOptionPane.showMessageDialog(null,"Data has been changed succesfully");                
                }
                else
                {
                //JOptionPane.showMessageDialog(null, "Data failed to update");
                }
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex);
            }
        }
        
        String f5 = txt_idtrans.getText();
        String f6a = txt_qty.getText();
        String f6b = txt_qty2.getText();
        String f7a = txt_idpants.getText();
        String f7b = txt_idclothes.getText();
        int i1 = Integer.parseInt(txt_stock.getText());
        int i2 = Integer.parseInt(txt_stock2.getText());
        int ii1 = Integer.parseInt(txt_qty.getText());
        int ii2 = Integer.parseInt(txt_qty2.getText());
        int iii1 = i1-ii1;
        int iii2 = i2-ii2;
        if (((f6a != null) && (f6b != null))&&((!"".equals(f6a)) && (!"".equals(f6b)))&&((!"0".equals(f6a)) && (!"0".equals(f6b)))){
            try {
                System.out.println("1");
                conn = new KONEKSI().getConnection();
                stmt = conn.createStatement();
                sql = "INSERT INTO `transaction_detail` (`id_transaction`, `id_product`, `qty`) VALUES ('"+f5+"', '"+f7a+"', '"+f6a+"'), ('"+f5+"', '"+f7b+"', '"+f6b+"')";
                int i = stmt.executeUpdate(sql);
                    if (i != 0) {
                        //BARANG PANTS
                        sql = "UPDATE product SET stock_product = '"+iii1+"' WHERE product.id_product = '"+f7a+"';";
                        insert_table(sql);
                        //BARANG CLOTHES
                        sql = "UPDATE product SET stock_product = '"+iii2+"' WHERE product.id_product = '"+f7b+"';";
                        insert_table(sql);
                    } else {
                        //JOptionPane.showMessageDialog(null, "Data failed to update");
                    }
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex);
            }
        } else if (!"0".equals(f6a) && f6a != null) {
            try {
                System.out.println("2");
                conn = new KONEKSI().getConnection();
                stmt = conn.createStatement();
                //System.out.print(checkidnow());
                //rs = stmt.executeQuery("");
                sql = "INSERT INTO `transaction_detail` (`id_transaction`, `id_product`, `qty`) VALUES ('"+f5+"', '"+f7a+"', '"+f6a+"')";
                int i = stmt.executeUpdate(sql);
                if (i != 0) {
                        //BARANG PANTS
                        sql = "UPDATE product SET stock_product = '"+iii1+"' WHERE product.id_product = '"+f7a+"';";
                        insert_table(sql);
                } else {
                        //JOptionPane.showMessageDialog(null, "Data failed to update");
                    }
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex);
            }
        } else if (!"0".equals(f6b) && f6b != null) {
            try {
                System.out.println("3");
                conn = new KONEKSI().getConnection();
                stmt = conn.createStatement();
                //System.out.print(checkidnow());
                //rs = stmt.executeQuery("");
                sql = "INSERT INTO `transaction_detail` (`id_transaction`, `id_product`, `qty`) VALUES ('"+f5+"', '"+f7b+"', '"+f6b+"')";
                int i = stmt.executeUpdate(sql);
                if (i != 0) {
                        //BARANG CLOTHES
                        sql = "UPDATE product SET stock_product = '"+iii2+"' WHERE product.id_product = '"+f7b+"';";
                        insert_table(sql);
                } else {
                        //JOptionPane.showMessageDialog(null, "Data failed to update");
                    }
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex);
            }
        } else {
            System.out.println("4");
            JOptionPane.showMessageDialog(null, "Qty of Each Product must be not null");
        }
        
        etc struk = new etc(txt_idtrans.getText(),txt_pembayaran.getText(),txt_kembalian.getText());
        area.setText(struk.viewstruk());
        
        txt_total.setText(checktotal(txt_idtrans.getText()));
        txt_qty.setText("0");
        txt_qty2.setText("0");
        table_plis(txt_idtrans.getText());
    }//GEN-LAST:event_btn_addActionPerformed

    private void btn_delActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_delActionPerformed
        
        try {
            conn = new KONEKSI().getConnection();
            stmt = conn.createStatement();
            sql = "DELETE FROM `transaction_detail` WHERE `id_transaction`=\""+ txt_idtrans.getText() +"\" AND `id_product` = '"+ txt_.getText() +"';";
            int i = stmt.executeUpdate(sql);
            if (i != 0)
            {
                sql = "UPDATE product SET stock_product = '"+txt_2.getText()+"' WHERE product.id_product = '"+ txt_.getText() +"';";
                insert_table(sql);
            }
            else
            {
                
            }
            conn.close();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        txt_total.setText(checktotal(txt_idtrans.getText()));
        txt_qty.setText("0");
        txt_qty2.setText("0");
        table_plis(txt_idtrans.getText());
    }//GEN-LAST:event_btn_delActionPerformed

    private void btn_okActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_okActionPerformed
        
        try {                                       
            
            etc struk = new etc(txt_idtrans.getText(),txt_pembayaran.getText(),txt_kembalian.getText());
            area.setText(struk.viewstruk());
            area.print();
            
            try {
                
                reset_txt();
                txt_idtrans.setText(checkid());
                txt_idtransnow.setText(checkidnow());
                table_plis(txt_idtrans.getText());
            } catch (Exception ex) {
                System.out.println(ex);
            }
        } catch (PrinterException ex) {
            Logger.getLogger(transactions.class.getName()).log(Level.SEVERE, null,ex);
        }
    }//GEN-LAST:event_btn_okActionPerformed

    private void txt_idcustActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_idcustActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_idcustActionPerformed

    private void choice1PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_choice1PropertyChange
        
    }//GEN-LAST:event_choice1PropertyChange

    private void choice1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_choice1MouseClicked
        //System.out.println(choice1.getSelectedItem());
    }//GEN-LAST:event_choice1MouseClicked

    private void choice1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_choice1ItemStateChanged
        //System.out.println(choice1.getSelectedItem());
    }//GEN-LAST:event_choice1ItemStateChanged

    private void choice1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_choice1MouseReleased
        //System.out.println(choice1.getSelectedItem());
    }//GEN-LAST:event_choice1MouseReleased

    private void choice1ComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_choice1ComponentResized
        //System.out.println(choice1.getSelectedItem());
    }//GEN-LAST:event_choice1ComponentResized

    private void choice1CaretPositionChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_choice1CaretPositionChanged
        //System.out.println(choice1.getSelectedItem());
    }//GEN-LAST:event_choice1CaretPositionChanged

    private void choice1ComponentMoved(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_choice1ComponentMoved
        //System.out.println(choice1.getSelectedItem());
    }//GEN-LAST:event_choice1ComponentMoved

    private void choice1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_choice1FocusLost
        txt_price.setText(checkprice(choice1.getSelectedItem()));
        txt_idpants.setText(checkidproduct(choice1.getSelectedItem()));
        txt_stock.setText(checkstock(choice1.getSelectedItem()));
    }//GEN-LAST:event_choice1FocusLost

    private void choice1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_choice1FocusGained
        
    }//GEN-LAST:event_choice1FocusGained

    private void choice2FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_choice2FocusGained
        
    }//GEN-LAST:event_choice2FocusGained

    private void choice2FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_choice2FocusLost
        txt_price2.setText(checkprice(choice2.getSelectedItem()));
        txt_idclothes.setText(checkidproduct(choice2.getSelectedItem()));
        txt_stock2.setText(checkstock(choice2.getSelectedItem()));
    }//GEN-LAST:event_choice2FocusLost

    private void choice2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_choice2MousePressed
        txt_price2.setText(checkprice(choice2.getSelectedItem()));
        txt_idclothes.setText(checkidproduct(choice2.getSelectedItem()));
        txt_stock2.setText(checkstock(choice2.getSelectedItem()));
    }//GEN-LAST:event_choice2MousePressed

    private void choice2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_choice2MouseExited
        
    }//GEN-LAST:event_choice2MouseExited

    private void choice1ComponentHidden(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_choice1ComponentHidden
        // TODO add your handling code here:
    }//GEN-LAST:event_choice1ComponentHidden

    private void choice2ComponentHidden(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_choice2ComponentHidden
        //txt_price2.setText(checkprice(choice2.getSelectedItem()));
    }//GEN-LAST:event_choice2ComponentHidden

    private void txt_idpantsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_idpantsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_idpantsActionPerformed

    private void txt_idclothesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_idclothesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_idclothesActionPerformed

    private void table_transactionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_transactionMouseClicked
        int i = table_transaction.getSelectedRow();
        TableModel model = table_transaction.getModel();
        txt_idclothes.setText(checkidproduct(choice2.getSelectedItem()));
        txt_0.setText(model.getValueAt(i, 0).toString());
        txt_1.setText(model.getValueAt(i, 1).toString());
        txt_2.setText(model.getValueAt(i, 2).toString());
        txt_3.setText(model.getValueAt(i, 3).toString());
        txt_.setText(checkidproduct(txt_0.getText()));
    }//GEN-LAST:event_table_transactionMouseClicked

    private void txt_1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_1ActionPerformed

    private void txt_pembayaranKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_pembayaranKeyTyped
        
    }//GEN-LAST:event_txt_pembayaranKeyTyped

    private void txt_pembayaranKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_pembayaranKeyReleased
        try {
            int f1 = Integer.parseInt(txt_pembayaran.getText());
            int f2 = Integer.parseInt(txt_total.getText());
            int f3 = f1-f2;
            txt_kembalian.setText(String.valueOf(f3));
        } catch (NumberFormatException ex) {
            System.out.println(ex);
        }
    }//GEN-LAST:event_txt_pembayaranKeyReleased

    private void txt_stockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_stockActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_stockActionPerformed

    private void txt_stock2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_stock2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_stock2ActionPerformed

    private void choice1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_choice1MousePressed
        txt_price.setText(checkprice(choice1.getSelectedItem()));
        txt_idpants.setText(checkidproduct(choice1.getSelectedItem()));
        txt_stock.setText(checkstock(choice1.getSelectedItem()));
    }//GEN-LAST:event_choice1MousePressed

    private void choice2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_choice2MouseClicked
        
    }//GEN-LAST:event_choice2MouseClicked

    private void txt_qty2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_qty2KeyReleased
        try {
            int f1 = Integer.parseInt(txt_stock2.getText());
            int f2 = Integer.parseInt(txt_qty2.getText());
            int f3 = f1-f2;
            if (f3<0){
                txt_qty2.setText("0");
                JOptionPane.showMessageDialog(null, "Quantity Exceeds Current Stock!\n"
                        + "Quantity must equal or less than "+f1);
            }
        } catch (NumberFormatException ex) {
            System.out.println(ex);
        }
    }//GEN-LAST:event_txt_qty2KeyReleased

    private void txt_qtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_qtyKeyReleased
        try {
            int f1 = Integer.parseInt(txt_stock.getText());
            int f2 = Integer.parseInt(txt_qty.getText());
            int f3 = f1-f2;
            if (f3<0){
                txt_qty.setText("0");
                JOptionPane.showMessageDialog(null, "Quantity Exceeds Current Stock!\n"
                        + "Quantity must equal or less than "+f1);
            }
        } catch (NumberFormatException ex) {
            System.out.println(ex);
        }
    }//GEN-LAST:event_txt_qtyKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(transactions.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(transactions.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(transactions.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(transactions.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new transactions().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea area;
    private javax.swing.JButton btn_add;
    private javax.swing.JButton btn_del;
    private javax.swing.JButton btn_ok;
    private javax.swing.JButton btn_reset;
    private java.awt.Choice choice1;
    private java.awt.Choice choice2;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator16;
    private javax.swing.JSeparator jSeparator17;
    private javax.swing.JSeparator jSeparator20;
    private javax.swing.JSeparator jSeparator25;
    private javax.swing.JSeparator jSeparator26;
    private javax.swing.JSeparator jSeparator27;
    private javax.swing.JSeparator jSeparator28;
    private javax.swing.JSeparator jSeparator29;
    private javax.swing.JSeparator jSeparator30;
    private javax.swing.JSeparator jSeparator31;
    private javax.swing.JSeparator jSeparator32;
    private javax.swing.JSeparator jSeparator33;
    private javax.swing.JSeparator jSeparator34;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JTabbedPane jTabbedPane1;
    private java.awt.Panel panel1;
    private javax.swing.JTable table_transaction;
    private javax.swing.JTextField txt_;
    private javax.swing.JTextField txt_0;
    private javax.swing.JTextField txt_1;
    private javax.swing.JTextField txt_2;
    private javax.swing.JTextField txt_3;
    private javax.swing.JTextField txt_idclothes;
    private javax.swing.JTextField txt_idcust;
    private javax.swing.JTextField txt_idemp;
    private javax.swing.JTextField txt_idpants;
    private javax.swing.JTextField txt_idtrans;
    private javax.swing.JTextField txt_idtransnow;
    private javax.swing.JTextField txt_kembalian;
    private javax.swing.JTextField txt_pembayaran;
    private javax.swing.JTextField txt_price;
    private javax.swing.JTextField txt_price2;
    private javax.swing.JTextField txt_qty;
    private javax.swing.JTextField txt_qty2;
    private javax.swing.JTextField txt_stock;
    private javax.swing.JTextField txt_stock2;
    private javax.swing.JTextField txt_total;
    // End of variables declaration//GEN-END:variables

    @Override
    public void insert_table(String sql) {
        try {
            conn = new KONEKSI().getConnection();
            stmt = conn.createStatement();
            int i = stmt.executeUpdate(sql);
            if (i != 0)
            {
                //JOptionPane.showMessageDialog(null,"Data has been changed succesfully");                
            }
            else
            {
                //JOptionPane.showMessageDialog(null, "Data failed to update");
            }
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(transactions.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void viewed_table(String sql) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update_table(String sql) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete_table(String sql) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String reset_txt() {
        try {
            txt_idcust.setText("");
            table_plis(txt_idtrans.getText());
            txt_total.setText("0");
            txt_stock.setText("0");
            txt_stock2.setText("0");
            txt_idpants.setText("-");
            txt_idclothes.setText("-");
            txt_price.setText("-");
            txt_price2.setText("-");
            txt_kembalian.setText("0");
            txt_pembayaran.setText("0");
            txt_qty.setText("0");
            txt_qty2.setText("0");
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return null;
    }

    @Override
    public String reset_tbl() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String reset_btn() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String display_tbl(String sql) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String check_idtrans(String f1, String f3) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String check_idproduct() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String check_idcustomer() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String check_idemployee() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String check_idsupplier() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
