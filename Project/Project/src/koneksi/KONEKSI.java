package koneksi;
import java.sql.*;

public class KONEKSI {
    
    private Connection connection = null;
    public Connection getConnection() {
       {
           try
            {
               connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/lookeddb?useTimezone=true&serverTimezone=UTC", "root", "");
               System.out.println("SUCCED to create the database connection.");
            }
            catch (SQLException ex)
            {
               System.out.println("Failed to create the database connection.");
            }
       }
       return connection;
    }
}
