-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 10, 2020 at 11:27 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lookeddb`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id_customer` varchar(6) NOT NULL,
  `name_customer` varchar(40) NOT NULL,
  `email_customer` varchar(35) NOT NULL,
  `address_customer` varchar(200) NOT NULL,
  `phone_number` varchar(14) NOT NULL,
  `gender` set('Perempuan','Laki-Laki','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id_customer`, `name_customer`, `email_customer`, `address_customer`, `phone_number`, `gender`) VALUES
('C001', 'kinantii', 'kinanti@gmail.com', 'jl kemana mana ', '081268888888', 'Laki-Laki'),
('C004', 'Trisya', 'trisya@gmail.com', 'jl. pondok indah no.11 ', '087853876345', 'Perempuan'),
('C005', 'Dita', 'Dita@gmail.com', 'jl. Raya Bogor no.20, Bogor ', '087853872569', 'Perempuan'),
('C007', 'Refina ', 'refina@gmail.com', 'jl. Suramadu no 4, jakarta Pusat ', '087853456789', 'Perempuan'),
('C010', 'Sukimin', 'sukiminyangselaluceria@yahoo.com', 'jangankepoya', '08125555856461', 'Laki-Laki'),
('C012', 'kinantii', 'kinanti@gmail.com', 'jl kemana mana ', '081265523698', 'Perempuan'),
('C013', 'kinantii', 'kinanti@gmail.com', 'jl kemana mana ', '081268888888', 'Perempuan');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id_employee` varchar(6) NOT NULL,
  `name_employee` varchar(40) NOT NULL,
  `address_employee` varchar(200) NOT NULL,
  `phone_employee` varchar(14) NOT NULL,
  `position` varchar(40) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id_employee`, `name_employee`, `address_employee`, `phone_employee`, `position`, `username`, `password`) VALUES
('E001', 'Suci 1', 'jl.citayem', '08153389568', 'Admin', 'admin', 'admin123'),
('E002', 'Suci 2', 'jl.citayem', '08153389568', 'Admin', 'admin', 'admin123'),
('E003', 'LOH SUCI LAGI', 'jl.citayem', '08153389568', 'Manager', 'Manager', 'manager123'),
('E004', 'Suci R1', 'jl.citayem', '08153389568', 'Cashier', 'lala', 'lala123'),
('E005', 'HAHAHA', 'jl.citayem', '08153389568', 'Cashier', 'lala', 'lala123'),
('E006', 'Suci R', 'jl.citayem', '08153389568', 'Cashier', 'lala', 'lala123'),
('E007', 'Suci', 'jl.citayem', '08153389568', 'Cashier', 'lala', 'lala123'),
('E008', 'SURYAaaaaaaaaaaaa', 'HUH', '081212121', 'admin', 'Manager', 'manager123');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id_product` varchar(6) NOT NULL,
  `name_product` varchar(50) NOT NULL,
  `type_product` set('Clothes','Pants','','') NOT NULL,
  `price_product` int(10) NOT NULL,
  `ukuran_product` varchar(6) NOT NULL,
  `stock_product` int(10) NOT NULL,
  `id_supplier` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id_product`, `name_product`, `type_product`, `price_product`, `ukuran_product`, `stock_product`, `id_supplier`) VALUES
('P001', 'Baggy Pants White', 'Pants', 1000000, 'M', 979, 'S001'),
('P002', 'Cloth Red', 'Pants', 100100, 'M', 92, 'S001'),
('P003', 'Cloth Pink', 'Clothes', 150000, 'L', 100, 'S001'),
('P004', 'Pants Jeans', 'Pants', 250000, 'M', 150, 'S001'),
('P005', 'Throusers Jeans', 'Clothes', 300000, 'L', 85, 'S001'),
('P006', 'Clothes Flower', 'Clothes', 150000, 'M', 0, 'S001'),
('P007', 'Pants Jeans', 'Pants', 250000, 'M', 131, 'S001'),
('P008', 'Pants Jeans', 'Pants', 300000, 'L', 200, 'S003'),
('P009', 'Baju sabrina', 'Clothes', 90000, 'L', 1000, 'S003');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id_supplier` varchar(6) NOT NULL,
  `name_supplier` varchar(30) NOT NULL,
  `type_supplier` varchar(30) NOT NULL,
  `phone_supplier` varchar(14) NOT NULL,
  `address_supplier` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id_supplier`, `name_supplier`, `type_supplier`, `phone_supplier`, `address_supplier`) VALUES
('S001', 'Uniqlo', 'Pants', '081295562358', 'jl. sudirman'),
('S002', 'Uniqlo', 'Clothes', '081295562358', 'jl. sudirman'),
('S003', 'H&M', 'Clothes', '081268895689', 'jl. Blok M');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id_transaction` varchar(6) NOT NULL,
  `id_employee` varchar(6) NOT NULL,
  `id_customer` varchar(6) DEFAULT NULL,
  `date_transcaction` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`id_transaction`, `id_employee`, `id_customer`, `date_transcaction`) VALUES
('T00001', 'E001', NULL, '2020-07-02'),
('T00002', 'E001', NULL, '2020-07-02'),
('T00003', 'E001', NULL, '2020-07-02'),
('T00004', 'E001', NULL, '2020-07-02'),
('T00005', 'E001', NULL, '2020-07-02'),
('T00006', 'E001', NULL, '2020-07-03'),
('T00007', 'E001', NULL, '2020-07-03'),
('T00008', 'E001', NULL, '2020-07-03'),
('T00009', 'E004', NULL, '2020-07-03'),
('T00010', 'E004', NULL, '2020-07-03'),
('T00011', 'E004', NULL, '2020-07-03'),
('T00012', 'E004', NULL, '2020-07-03'),
('T00013', 'E004', NULL, '2020-07-03'),
('T00014', 'E004', 'C010', '2020-07-03'),
('T00015', 'E004', NULL, '2020-07-07'),
('T00016', 'E004', NULL, '2020-07-07'),
('T00017', 'E004', NULL, '2020-07-07'),
('T00018', 'E004', NULL, '2020-07-07'),
('T00019', 'E004', NULL, '2020-07-07'),
('T00020', 'E004', NULL, '2020-07-07'),
('T00021', 'E004', NULL, '2020-07-07'),
('T00022', 'E001', NULL, '2020-07-10'),
('T00023', 'E001', NULL, '2020-07-10'),
('T00024', 'E001', NULL, '2020-07-10'),
('T00025', 'E001', NULL, '2020-07-10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id_employee`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id_product`),
  ADD KEY `id_supplier` (`id_supplier`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id_transaction`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_employee` (`id_employee`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`id_supplier`) REFERENCES `supplier` (`id_supplier`);

--
-- Constraints for table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `transaction_ibfk_1` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id_customer`),
  ADD CONSTRAINT `transaction_ibfk_2` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id_employee`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
